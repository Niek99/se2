//
// Created by Niek on 24-10-2018.
//

#include <stdexcept>
#include "Calculator.h"
#include <iostream>

namespace Niek{
    Calculator::Calculator() {}

    int Calculator::add(int a, int b) {
        return a+b;
    }

    double Calculator::add(double a, double b) {
        return a+b;
    }

    int Calculator::substract(int a, int b) {
        return a-b;
    }

    double Calculator::substract(double a, double b) {
        return a-b;
    }

    int Calculator::multiply(int a, int b) {
        return a*b;
    }

    double Calculator::multiply(double a, double b) {
        return a*b;
    }

    int Calculator::divide(int a, int b) {
        try {
            if (b == 0) {
                throw std::logic_error("Unable to divide a number by 0");
            } else {
                return a / b;
            }
        } catch(std::logic_error e){
            std::cout<< "Exception: " << e.what() << std::endl;
        }
        return b;
    }

    double Calculator::divide(double a, double b) {
        try {
            if (b == 0.0) {
                throw std::logic_error("Unable to divide a number by 0");
            } else {
                return a / b;
            }
        } catch(std::logic_error e){
            std::cout<< "Exception: " << e.what() << std::endl;
        }
        return b;
    }

    int Calculator::square(int a) {
        return a*a;
    }

    double Calculator::square(double a) {
        return a*a;
    }
}
//
// Created by Niek on 24-10-2018.
//

#ifndef SECALCULATOR_CALCULATOR_H
#define SECALCULATOR_CALCULATOR_H

namespace Niek {
    class Calculator {

    public:
        Calculator();

        int add(int a, int b);

        double add(double a, double b);

        int substract(int a, int b);

        double substract(double a, double b);

        int multiply(int a, int b);

        double multiply(double a, double b);

        int divide(int a, int b);

        double divide(double a, double b);

        int square(int a);

        double square(double a);

    };
}

#endif //SECALCULATOR_CALCULATOR_H

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/Niek/se2/SEcalculator/Calculator.cpp" "C:/Users/Niek/se2/SEcalculator/cmake-build-debug/CMakeFiles/SEcalculator.dir/Calculator.cpp.obj"
  "C:/Users/Niek/se2/SEcalculator/main.cpp" "C:/Users/Niek/se2/SEcalculator/cmake-build-debug/CMakeFiles/SEcalculator.dir/main.cpp.obj"
  "C:/Users/Niek/se2/SEcalculator/tests.cpp" "C:/Users/Niek/se2/SEcalculator/cmake-build-debug/CMakeFiles/SEcalculator.dir/tests.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/googletest-master/googlemock/include"
  "../lib/googletest-master/googletest/include"
  "../lib/googletest-master/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/Users/Niek/se2/SEcalculator/cmake-build-debug/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "C:/Users/Niek/se2/SEcalculator/cmake-build-debug/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

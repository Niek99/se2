//
// Created by Niek on 26-10-2018.
//

#include "Calculator.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using testing::Eq;

namespace {
    class Classdeclaration : public testing::Test{
    public:
        Niek::Calculator obj;
        Classdeclaration(){
            obj;
        }
    };
}

TEST_F(Classdeclaration, nameOfTheTest1){
    ASSERT_EQ(7,obj.add(2,5));
}

TEST_F(Classdeclaration, nameOfTheTest2){
    ASSERT_EQ(7.5,obj.add(2.5,5.0));
}

TEST_F(Classdeclaration, nameOfTheTest3){
    ASSERT_EQ(3,obj.substract(5,2));
}

TEST_F(Classdeclaration, nameOfTheTest4){
    ASSERT_EQ(2.5,obj.substract(5.0,2.5));
}

TEST_F(Classdeclaration, nameOfTheTest5){
    ASSERT_EQ(20,obj.multiply(4,5));
}

TEST_F(Classdeclaration, nameOfTheTest6){
    ASSERT_EQ(7,obj.multiply(2.0,3.5));
}

TEST_F(Classdeclaration, nameOfTheTest7){
    ASSERT_EQ(2,obj.divide(10,5));
}

TEST_F(Classdeclaration, nameOfTheTest8){
    ASSERT_EQ(3.0,obj.divide(6.0,2.0));
}

TEST_F(Classdeclaration, nameOfTheTest9){
    ASSERT_EQ(25,obj.square(5));
}

TEST_F(Classdeclaration, nameOfTheTest10){
    ASSERT_EQ(9.0,obj.square(3.0));
}

//error handling testen
TEST_F(Classdeclaration, nameOfTheTest11){
    ASSERT_EQ(0,obj.divide(2,0));
}

TEST_F(Classdeclaration, nameOfTheTest12){
    ASSERT_EQ(0.0,obj.divide(2.0,0.0));
}